const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  publicPath: './',//打包的文件路径
  lintOnSave: false,//代码规范问题
})
